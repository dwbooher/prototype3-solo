using UnityEngine;
using System.Collections;

public class BackgroundGenerator : MonoBehaviour
{
    public Texture[] PlanetTexture;
    public float ScrollSpeed = 0.5f;
    public float Offset;


    public float myTimer = 5.0f;
    public bool EnableChange = false;

    //Use this for initialization
    void Start()
    {



    }

    //Update is called once per frame
    void Update()
    {

        if (myTimer > 0)
        {
            myTimer -= Time.deltaTime;
            EnableChange = true;
        }
        if (myTimer <= 0 && EnableChange == true)
        {
            Debug.Log("Timer is Less than Zero");
            #region Random Background Selector

            int RandomPlanetTexture = Random.Range(0, PlanetTexture.Length);
            GetComponent<Renderer>().material.mainTexture = PlanetTexture[RandomPlanetTexture];

            EnableChange = false;
            myTimer = 5.0f;
            #endregion

        }


        #region Background Texture Scrolling

        //Set the scrolling speed of the texture
        Offset += Time.deltaTime * ScrollSpeed;

        //Set the axis on which the background should scroll
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.0f, Offset);

        #endregion

    }

}
